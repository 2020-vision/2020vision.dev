import React, { Component } from 'react'
import BodyView from '../components/BodyView'
import FooterView from '../components/FooterView'
import NavView from '../components/NavView'

export default class extends Component {
  render() {
    return (
      <div>
        <NavView/>
        <BodyView/>
        <FooterView/>
      </div>
    )
  }
}