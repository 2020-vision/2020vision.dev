export default () => (
  <nav className="view-nav">
    <div>
      <p><span>2020</span> Vision</p>
      <ul>
        <li>Home</li>
        <li>About</li>
        <li>Portfolio</li>
        <li>Contact</li>
        <li>ENG</li>
      </ul>
    </div>
  </nav>
)