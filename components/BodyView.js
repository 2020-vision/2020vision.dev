import Head from 'next/head'
import '../styles/main.scss'

export default () => (
  <div>
    <Head>
      <title>2020Vision</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    </Head>
    <header>
      <div className="wrapper">
        <div className="logo">
          <img className="fit" src="./static/2020vision.svg" alt="logo"/>
        </div>
        <h1>You envision. We create.</h1>
      </div>
     <p>AN EXPERIENCED TEAM OF DEDICATED SOFTWARE ENGINEERS, UI/UX DESIGNERS, AND BUSINESS CONSULTANTS READY TO <br></br> PROVIDE INDUSTRY 4.0 GRADE SOLUTIONS. WE WILL ELIMINATE ANY OBSTACLES FOR YOU OR YOUR BUSINESS.</p>
     <div className="buttons">
       <button className="btn outline">DISCOVER OUR WORK</button>
       <button className="btn">GET IN TOUCH</button>
     </div>
    </header>
  </div>
)